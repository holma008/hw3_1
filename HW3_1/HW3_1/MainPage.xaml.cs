﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3_1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void NavigateToNewPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new Page1(Entry.Text));
        }
    }
}
